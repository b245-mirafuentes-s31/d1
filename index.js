// Use the "require" directive to load Node.js modules
// A "module" is a software component or part of a program that contains one or more routines
// "http module" lets Node.js transfer data using the Hyper Text Transfer Protocol

let http = require("http");

http.createServer(function(req, response){

	// Use the writeHead() method to:
	// Set a status code for the response, a 200 means ok
	// Set the content-type of the response as a plain text message
	response.writeHead(200, {'Content-Type': 'text/plain'})
	response.end("Hello, B245!")

}).listen(4000)

// When server is running, console will print the message;
console.log("Server is running at local host: 4000")